module gitee.com/ifinder/finder-kafka-go

go 1.14

require (
	github.com/Shopify/sarama v1.30.0
	github.com/alecthomas/log4go v0.0.0-20180109082532-d146e6b86faa
	github.com/cnfinder/sarama-cluster v1.0.3-0.20211117070552-ffd73cf7e86d
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.17.0 // indirect
)
